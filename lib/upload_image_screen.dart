import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jehad_project/utils/helpers.dart';

import 'fb_storage_controller.dart';

class UploadImageScreen extends StatefulWidget {
  const UploadImageScreen({Key? key}) : super(key: key);

  @override
  _UploadImageScreenState createState() => _UploadImageScreenState();
}

class _UploadImageScreenState extends State<UploadImageScreen> with Helpers{
  ImagePicker _imagePicker = ImagePicker();
  XFile? _pickedFile;
  double? _progressValue = 0;
  var url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('UPLOAD'),
      ),
      body: Column(
        children: [
          LinearProgressIndicator(
            value: _progressValue,
            minHeight: 8,
          ),
          Container(
            child: _pickedFile != null
              ? Image.file(File(_pickedFile!.path))
              : TextButton(
                  onPressed: () async {
                    await pickImage();
                  },
                  child: Text('PICK IMAGE'),
                  style: TextButton.styleFrom(
                    minimumSize: Size(double.infinity, 0),
                  ),
                ),
          ),
          ElevatedButton.icon(
            onPressed: () async {
              await uploadImage();
            },
            icon: Icon(Icons.cloud_upload_outlined),
            label: Text('UPLOAD'),
            style: ButtonStyle(
              minimumSize: MaterialStateProperty.all(
                Size(double.infinity, 50),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> pickImage() async {
    _pickedFile = await _imagePicker.pickImage(source: ImageSource.gallery);
    // _pickedFile = await _imagePicker.getImage(source: ImageSource.gallery);
    if (_pickedFile != null) {
      setState(() {});
    }
  }

  Future<void> uploadImage() async {
    if (_progressValue != null) {
      if (_pickedFile != null) {
        changeProgressValue(null);
        FbStorageController().uploadImage(
          file: File(_pickedFile!.path),
          uploadEvent: (bool status, TaskState taskState, Reference? reference) async{
            if (status) {
              changeProgressValue(1);
              showSnackBar(context: context, message: 'Uploaded successfully');
              url = Uri.parse(await reference!.getDownloadURL());

            } else {
              if (taskState == TaskState.error) {
                changeProgressValue(0);
                showSnackBar(
                    context: context,
                    message: 'Upload failed, try again',
                    error: true);
              } else {
                changeProgressValue(null);
              }
            }
          },
        );
      }
    } else {
      print('CURRENTLY UPLOADING FILE');
    }
  }

  void changeProgressValue(double? value) {
      setState(() {
        _progressValue = value;
      });
  }
}
