import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jehad_project/app/levels_screen/view/add_levels_screen.dart';
import 'package:jehad_project/upload_image_screen.dart';

import 'app/launch_screen/launch_screen.dart';
import 'app/levels_screen/components/levels_widget.dart';
import 'app/levels_screen/view/levels_screen.dart';
import 'app/on_boarding/on_boarding_screen.dart';
import 'app/subject/add_subject_screen.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 844),
      builder: () =>
          MaterialApp(
            initialRoute: 'launch_screen',
            // home: AddSubjectScreen(classId: 'l',),
            debugShowCheckedModeBanner: false,
            locale: Locale('ar'),
            routes: {
              'launch_screen': (_) => LaunchScreen(),
              'on_boarding_screen': (_) => OnBoardingScreen(),
              'levels_screen': (_) => LevelsScreen(),
              'add_levels_screen': (_) => AddLevelsScreen()
            },
          ),
    );
  }
}
