import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppTextWidget extends StatelessWidget {
  final String content;
  final Color color;
  final FontWeight fontWeight;
  final double fontSize;
  final String? fontFamily;
  final TextAlign textAlign;
  final TextDecoration textDecoration;
  final Color decorationColor;

  AppTextWidget({
    required this.content,
    required this.fontSize,
    this.color = Colors.black,
    this.fontWeight = FontWeight.normal,
    this.textAlign = TextAlign.center,
    this.textDecoration = TextDecoration.none,
    this.decorationColor = Colors.transparent,
    this.fontFamily = 'cairo',
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      content,
      textAlign: textAlign,
      style: TextStyle(
        decoration: textDecoration,
        decorationColor: decorationColor,
        color: color,
        fontWeight: fontWeight,
        fontSize: sizeFont(fontSize),
        fontFamily: fontFamily),
      );
  }


   double sizeFont(double size) {
    return size.sp;
   }
}
