
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'app_text_widget.dart';

class AppTextButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color buttonColor;
  final FontWeight fontWeight;
  final double? fontSize;
  final String? fontFamily;
  final TextAlign textAlign;
  final void Function() onPressed;

  AppTextButton(
      {required this.text,
        this.textColor = Colors.white,
        this.fontWeight = FontWeight.normal,
        this.textAlign = TextAlign.start,
        this.buttonColor = Colors.white,
        required this.onPressed,
        this.fontFamily = 'cairo',
        this.fontSize,});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: AppTextWidget(
        content: text,
        color: textColor,
        fontWeight: fontWeight,
        fontSize: fontSize == null ? 15.sp : sizeFont(fontSize!),
        fontFamily: fontFamily),
      style: ElevatedButton.styleFrom(
        primary: buttonColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
      onPressed: onPressed

    );
  }


  double sizeFont(double size) {
    return size.sp;
  }

}
