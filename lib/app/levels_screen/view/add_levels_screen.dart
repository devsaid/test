import 'package:flutter/material.dart';
import 'package:jehad_project/app/levels_screen/firebase/levels_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/model/levels.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/utils/helpers.dart';
import 'package:jehad_project/widgets/app_elevated_button.dart';
import 'package:jehad_project/widgets/app_text_field.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

class AddLevelsScreen extends StatefulWidget {


  @override
  _AddLevelsScreenState createState() => _AddLevelsScreenState();
}

class _AddLevelsScreenState extends State<AddLevelsScreen> with Helpers {


  late TextEditingController levelsTitleController;
  late TextEditingController levelsDescriptionController;

  @override
  void initState() {
    super.initState();
    levelsTitleController = TextEditingController();
    levelsDescriptionController  = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: primaryColor,
        centerTitle: true,
        title: AppTextWidget(content: 'أضافة مرحلة',fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold,),
      ),
      backgroundColor: secondaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              AppTextField(controller: levelsTitleController, label: 'اسم المرحلة'),
              SizedBox(height: 15,),
              AppTextField(controller: levelsDescriptionController, label: 'الوصف',line: 2,),
              SizedBox(height: 30,),
              AppElevatedButton(
                onPressed: () async {
                  await performSave();
                  Navigator.pop(context);
                },
                text: 'حــفـظ',
                fontFamily: 'cairo',
                buttonColor: primaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }


  Future<void> performSave() async {
    if (checkData()) {
      await save();
    }
  }

  bool checkData() {
    if (levelsTitleController.text.isNotEmpty &&
        levelsDescriptionController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter required data', error: true);
    return false;
  }

  Future<void> save() async {
    await LevelsFirebaseController().storeDocument(
      data: level,
      event: (bool status) {
        if (status) clear();
        String message = status ? 'Saved successfully' : 'Failed to save';
        showSnackBar(context: context, message: message, error: !status);
      },
    );
  }

  Level get level {
    Level level = Level();
    level.title = levelsTitleController.text;
    level.description = levelsDescriptionController.text;
    return level;
  }

  void clear() {
    levelsTitleController.text = '';
    levelsDescriptionController.text = '';
  }
}
