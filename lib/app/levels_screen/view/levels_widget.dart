import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jehad_project/app/levels_screen/model/levels.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

import 'add_class_screen.dart';

class LevelWidget extends StatelessWidget {

  final Level level;
  final void Function(String id) deleteFunc;
  final void Function(Level level) updateFunc;

  LevelWidget({required this.level, required this.deleteFunc, required this.updateFunc});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 2,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: EdgeInsets.all(13),
        child: Row(
          children: [

            IconButton(
              icon: Icon(Icons.delete),
              onPressed: ()=> deleteFunc(level.id),
            ),

            IconButton(
              icon: Icon(Icons.edit),
              onPressed: ()=> updateFunc(level),
            ),

            IconButton(
                icon: Icon(Icons.add_circle_outlined),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (c){
                    return AddClassScreen(levelsId: level.id);
                  }));
                }
            ),

            Spacer(),

            AppTextWidget(
              content: level.title,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),

            SizedBox(
              width: 8,
            ),

          ],
        ),
      ),
    );
  }
}
