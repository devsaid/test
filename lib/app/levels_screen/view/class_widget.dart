
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jehad_project/app/levels_screen/firebase/classes_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/model/class.dart';
import 'package:jehad_project/app/subject/subject_screen.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';


class ClassWidget extends StatelessWidget {
  final QueryDocumentSnapshot myClass;
  final void Function(String id) deleteFunc;

  ClassWidget({required this.myClass, required this.deleteFunc});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => navigateToSubjectsScreen(context: context,classID: myClass.get('class_id')),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 13.h),
        margin: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: secondaryColor,width: 2),
            boxShadow: [
              BoxShadow(color: Colors.grey.withOpacity(0.16),
                  spreadRadius: 1,
                  offset: Offset(0,0),
                  blurRadius: 3)
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [

                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: ()=> deleteFunc(myClass.get('class_id'))
                ),

                IconButton(
                  icon: Icon(Icons.edit),
                  // onPressed: () async => await delete(
                  //   levels.get('level_id'),

                  onPressed: (){},
                ),
                Spacer(),
                AppTextWidget(
                  content: myClass.get('title'),
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  textAlign: TextAlign.end,
                ),



              ],
            ),
            SizedBox(
              height: 10.h,
            ),
            AppTextWidget(
              content: myClass.get('description'),
              fontSize: 14,
              fontWeight: FontWeight.w300,
              textAlign: TextAlign.end,
            ),
          ],
        ),
      ),
    );
  }

  navigateToSubjectsScreen({required BuildContext context, required String classID}){ 
    Navigator.push(context, MaterialPageRoute(builder: (_)=> SubjectScreen(classId: classID,)));
  }


}
