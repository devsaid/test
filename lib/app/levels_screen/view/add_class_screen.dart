import 'package:flutter/material.dart';
import 'package:jehad_project/app/levels_screen/firebase/classes_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/model/class.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/utils/helpers.dart';
import 'package:jehad_project/widgets/app_elevated_button.dart';
import 'package:jehad_project/widgets/app_text_field.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

class AddClassScreen extends StatefulWidget {

 late String levelsId;

 AddClassScreen({required this.levelsId});

 @override
  _AddClassScreenState createState() => _AddClassScreenState();
}

class _AddClassScreenState extends State<AddClassScreen> with Helpers{

  late TextEditingController classTitleController;
  late TextEditingController classDescriptionController;

  @override
  void initState() {
    super.initState();
    classTitleController = TextEditingController();
    classDescriptionController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: primaryColor,
        centerTitle: true,
        title: AppTextWidget(content: 'أضافة صف',fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold,),
      ),
      backgroundColor: secondaryColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              AppTextField(controller: classTitleController, label: 'اسم الصف'),
              SizedBox(height: 15,),
              AppTextField(controller: classDescriptionController, label: 'وصف الصف',line: 2,),
              SizedBox(height: 30,),
              AppElevatedButton(
                onPressed: () async {
                  await performSave();

                },
                text: 'حــفـظ',
                fontFamily: 'cairo',
                buttonColor: primaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }


  Future<void> performSave() async {
    if (checkData()) {
      await save();
    }
  }

  bool checkData() {
    if (classTitleController.text.isNotEmpty &&
        classDescriptionController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter required data', error: true);
    return false;
  }

  Future<void> save() async {
    await ClassesFirebaseController().storeDocument(
      data: myClass,
      event: (bool status) {
        if (status) clear();
        String message = status ? 'Saved successfully' : 'Failed to save';
        showSnackBar(context: context, message: message, error: !status);
      },
    );
   
  }

  Class get myClass {
    Class myClass = Class();
    myClass.title = classTitleController.text;
    myClass.description = classDescriptionController.text;
    myClass.levelId = widget.levelsId;
    return myClass;
  }

  void clear() {
    classTitleController.text = '';
    classDescriptionController.text = '';
  }
}
