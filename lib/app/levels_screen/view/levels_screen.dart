import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jehad_project/app/levels_screen/firebase/classes_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/firebase/levels_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/model/levels.dart';
import 'package:jehad_project/app/levels_screen/view/add_class_screen.dart';
import 'package:jehad_project/app/levels_screen/view/edit_level_screen.dart';
import 'package:jehad_project/app/levels_screen/view/levels_widget.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/utils/helpers.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

import 'class_widget.dart';

class LevelsScreen extends StatefulWidget {
  @override
  _LevelsScreenState createState() => _LevelsScreenState();
}

class _LevelsScreenState extends State<LevelsScreen> with Helpers {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe1efef),

      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, 'add_levels_screen'),
        child: Icon(Icons.add),
        backgroundColor: primaryColor,
      ),

      body: SafeArea(
        child: Column(
          children: [

            Container(
              alignment: Alignment.center,
              height: 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(bottomRight: Radius.circular(40),bottomLeft: Radius.circular(30)),
                boxShadow: [
                  BoxShadow(color: Colors.black.withOpacity(0.16),
                  spreadRadius: 2,
                  offset: Offset(0,0),
                  blurRadius: 5)
                ]
              ),

              child: Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    AppTextWidget(
                      content:'مرحباً بك !',
                      fontFamily: 'cairo',
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.end,
                    ),

                    SizedBox(height: 10,),

                    AppTextWidget(
                      content: 'والحصول على كل ما يلزم اهلا وسهلا بكم في تطبيق تميز تستطيع الان تصفح التطبيق مجانا',
                      fontFamily: 'cairo',
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      textAlign: TextAlign.end,
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(height: 30,),

            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: LevelsFirebaseController().readCollection(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting)
                  {
                    return Center(child: CircularProgressIndicator());
                  }
                  else if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                    List<QueryDocumentSnapshot> docs = snapshot.data!.docs;
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: SingleChildScrollView(
                        child: ExpansionPanelList.radio(
                          animationDuration: Duration(milliseconds: 600),
                          expandedHeaderPadding: EdgeInsets.all(15),
                          dividerColor: secondaryColor,
                          elevation: 0,
                          children: docs.map((level) =>
                            ExpansionPanelRadio(
                              canTapOnHeader: true,
                              headerBuilder: (BuildContext context, bool isExpanded) {
                                return LevelWidget(level: getLevel(level),deleteFunc: deleteLevels,updateFunc: updateLevel,);
                              },
                          body: Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              StreamBuilder<QuerySnapshot>(
                              stream: ClassesFirebaseController().readCollection(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return Center(child: CircularProgressIndicator());
                                } else if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                                  List<QueryDocumentSnapshot> classes = snapshot.data!.docs;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 18),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: classes.where((element) => element.get('level_id') == level.get('level_id')).toList().map((e) => ClassWidget(myClass: e,deleteFunc: deleteClass)).toList()
                                    ),
                                  );
                                } else {
                                  return Text('no data');
                                }
                              })
                            ],
                          ),
                          value: level.get('level_id'),
                        )).toList(),
                        ),
                      ),
                    );
                  } else {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.warning,
                            size: 80,
                          ),
                          Text(
                            'NO DATA',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 28,
                            ),
                          )
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> deleteLevels(String id) async {
    await LevelsFirebaseController().deleteDocument(
      id: id,
      event: (bool status) {
        String message =
            status ? 'Deleted successfully' : 'Failed to delete post';
        showSnackBar(context: context, message: message);
      },
    );
  }

  void updateLevel(Level level){
    Navigator.push(context, MaterialPageRoute(builder: (_) => EditLevelScreen(level)));
  }

  Level getLevel(QueryDocumentSnapshot documentSnapshot) {
    Level level = Level();
    level.id = documentSnapshot.get('level_id');
    level.title = documentSnapshot.get('title');
    level.description = documentSnapshot.get('description');
    return level;
  }

  deleteClass(String id) async {
    await ClassesFirebaseController().deleteDocument(
      id: id,
      event: (bool status) {
        String message =
        status ? 'Deleted successfully' : 'Failed to delete post';
        showSnackBar(context: context, message: message);
      },
    );
  }

}
