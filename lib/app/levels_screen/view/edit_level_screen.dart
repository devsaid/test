import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jehad_project/app/levels_screen/firebase/levels_firestore_controller.dart';
import 'package:jehad_project/app/levels_screen/model/levels.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/utils/helpers.dart';
import 'package:jehad_project/widgets/app_elevated_button.dart';
import 'package:jehad_project/widgets/app_text_field.dart';

class EditLevelScreen extends StatefulWidget {

  final Level levelSelected;

  EditLevelScreen(this.levelSelected);

  @override
  _EditLevelScreenState createState() => _EditLevelScreenState();
}

class _EditLevelScreenState extends State<EditLevelScreen> with Helpers{


  late TextEditingController levelsTitleController;
  late TextEditingController levelsDescriptionController;

  @override
  void initState() {
    super.initState();

    levelsTitleController = TextEditingController(text: widget.levelSelected.title);
    levelsDescriptionController  = TextEditingController(text: widget.levelSelected.description);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppTextField(controller: levelsTitleController, label: 'title'),
            AppTextField(controller: levelsDescriptionController, label: 'descriptions',line: 5,),
            AppElevatedButton(
              onPressed: () async {
                await performUpdate();
              },
              text: 'Edit',
              buttonColor: primaryColor,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> performUpdate() async {
    if (checkData()) {
      await update();
    }
  }

  bool checkData() {
    if (levelsTitleController.text.isNotEmpty &&
        levelsDescriptionController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter required data', error: true);
    return false;
  }

  Future<void> update() async {
    await LevelsFirebaseController().updateDocument(
      data: level,
      event: (bool status) {
        if (status) Navigator.pop(context, 'Level updated successfully');
        String message =
        status ? 'Level Updated successfully' : 'Failed to update code';
        showSnackBar(context: context, message: message, error: !status);
      },
    );
  }

  Level get level {
    Level level = Level();
    level.id = widget.levelSelected.id;
    level.title = levelsTitleController.text;
    level.description = levelsDescriptionController.text;
    return level;
  }

}
