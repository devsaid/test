
class Level {
  late String id;
  late String title;
  late String description;
  Level();

  Level.fromMap(Map<String, dynamic> documentMap) {
    title = documentMap['title'];
    description = documentMap['description'];
    id = documentMap['id'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String,dynamic>();
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}