class Class{
  late String classId;
  late String levelId;
  late String title;
  late String description;
  Class();

  Class.fromMap(Map<String, dynamic> documentMap) {
    title = documentMap['title'];
    description = documentMap['description'];
    classId = documentMap['class_id'];
    levelId = documentMap['level_id'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String,dynamic>();
    map['title'] = title;
    map['description'] = description;
    map['level_id'] = levelId;
    return map;
  }
}