import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jehad_project/app/levels_screen/model/levels.dart';
import 'package:jehad_project/firebase_controller.dart';


class LevelsFirebaseController extends FirebaseController<Level>{

  @override
  Future<void> deleteDocument({required String id, required FirestoreEvent event,String? parentId}) async{
    await firebaseFirestore
        .collection('levels')
        .doc(id)
        .delete()
        .then((value) => event(true))
        .catchError((error) => event(false));
  }

  @override
  Stream<QuerySnapshot<Object?>> readCollection({String? parentId}) async* {
    yield* firebaseFirestore
        .collection('levels')
        .snapshots();
  }

  @override
  Stream<DocumentSnapshot<Map<String, dynamic>>> readDocument(String id) async*{
    yield* firebaseFirestore
        .collection('levels')
        .doc(id)
        .snapshots();
  }



  @override
  Future<void> updateDocument({required Level data, required FirestoreEvent event}) async{
        await firebaseFirestore
            .collection('levels')
            .doc(data.id)
            .update(data.toMap())
            .then((value) => event(true))
            .catchError((error) => event(false));
      }

  @override
  Future<void> storeDocument({required Level data, String? parentId, required FirestoreEvent event}) async{
    await firebaseFirestore
        .collection('levels')
        .add(data.toMap())
        .then((value) => value.update({'level_id': value.id}).then((value) => event(true)).catchError((error) => event(false))
    );
  }
}
