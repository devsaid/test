import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jehad_project/app/levels_screen/model/class.dart';
import 'package:jehad_project/firebase_controller.dart';


class ClassesFirebaseController extends FirebaseController<Class>{

  @override
  Future<void> deleteDocument({required String id, required FirestoreEvent event,String? parentId}) async{
    await firebaseFirestore
        .collection('classes')
        .doc(id)
        .delete()
        .then((value) => event(true))
        .catchError((error) => event(false));
  }

  @override
  Stream<QuerySnapshot<Object?>> readCollection({String? parentId}) async* {
    yield* firebaseFirestore
        .collection('classes')
        .snapshots();
  }

  @override
  Stream<DocumentSnapshot<Map<String, dynamic>>> readDocument(String id) async*{
    yield* firebaseFirestore
        .collection('classes')
        .doc(id)
        .snapshots();
  }


  // Future<void> storeDocument({required data, required FirestoreEvent event}) async{
  //   var i =0;
  //   await firebaseFirestore
  //       .collection('classes')
  //       .add(data.toMap())
  //       .then((value) => value.update({'class_id': value.id}).then((value) => event(true)).catchError((error) => event(false))
  //   );
  //   print('-------------------------------------- number of calls = ${++i}');
  // }

  @override
  Future<void> updateDocument({required Class data, required FirestoreEvent event}) {
    // TODO: implement updateDocument
    throw UnimplementedError();
  }

  @override
  Future<void> storeDocument({required Class data, String? parentId, required FirestoreEvent event}) async{
      await firebaseFirestore
          .collection('classes')
          .add(data.toMap())
          .then((value) => value.update({'class_id': value.id}).then((value) => event(true)).catchError((error) => event(false))
      );
  }


}