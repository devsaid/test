import 'package:flutter/material.dart';
import 'package:jehad_project/utils/colors.dart';

class LaunchScreen extends StatefulWidget {


  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushReplacementNamed(context, 'on_boarding_screen');
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
      body: Center(
        child: Text('Appliaction name'),
      ),
    );
  }
}
