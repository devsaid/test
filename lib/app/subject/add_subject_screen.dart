import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jehad_project/app/subject/subject.dart';
import 'package:jehad_project/app/subject/subject_firebase_controller.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/utils/helpers.dart';
import 'package:jehad_project/widgets/app_elevated_button.dart';
import 'package:jehad_project/widgets/app_text_field.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

import '../../fb_storage_controller.dart';

class AddSubjectScreen extends StatefulWidget {
  final String classId;

  AddSubjectScreen({required this.classId});

  @override
  _AddSubjectScreenState createState() => _AddSubjectScreenState();
}

class _AddSubjectScreenState extends State<AddSubjectScreen> with Helpers{
  late TextEditingController titleController;
  late TextEditingController descriptionController;
  var url;
  ImagePicker _imagePicker = ImagePicker();
  XFile? _pickedFile;
  double? _progressValue = 0;

  @override
  void initState() {
    titleController = TextEditingController();
    descriptionController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        centerTitle: true,
        title: AppTextWidget(content: 'أضافة مـادة',fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold,),
      ),
      backgroundColor: secondaryColor,

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: [
              LinearProgressIndicator(
                value: _progressValue,
                minHeight: 8,
                backgroundColor: secondaryColor,
                color: primaryColor,
              ),
              Container(
                height: 250,
                width: double.infinity,
                color: Colors.white,
                child: _pickedFile != null
                  ? Image.file(File(_pickedFile!.path),height: 250,width: 250,)
                  : TextButton(
                      onPressed: () async {
                        await pickImage();
                      },
                      child: AppTextWidget(content:'أضــافة صــورة',fontSize: 18,),
                      style: TextButton.styleFrom(
                        minimumSize: Size(double.infinity, 0),
                      ),
                    ),
              ),

              SizedBox(height: 50,),

              AppTextField(controller: titleController, label: 'أســم المـــادة'),
              SizedBox(height: 15,),
              AppTextField(controller: descriptionController, label: 'وصـــف المــادة',line: 2,),
              SizedBox(height: 30,),
              AppElevatedButton(text: 'اضـــافة',buttonColor: primaryColor, onPressed: () async => await uploadImage())
            ],
          ),
        ),
      ),
    );
  }

  Subject get subject{
    Subject subject = Subject();
    subject.title = titleController.text;
    subject.description = descriptionController.text;
    subject.image = url.toString();
    return subject;
  }

  Future<void> pickImage() async {
    _pickedFile = await _imagePicker.pickImage(source: ImageSource.gallery);
    if (_pickedFile != null) {
      setState(() {});
    }
  }

  Future<void> uploadImage() async {
    if (_progressValue != null) {
      if (_pickedFile != null) {
        changeProgressValue(null);
        FbStorageController().uploadImage(
          file: File(_pickedFile!.path),
          uploadEvent: (bool status, TaskState taskState, Reference? reference) async{
            if (status) {
              changeProgressValue(1);
              showSnackBar(context: context, message: 'Uploaded successfully');
              url = Uri.parse(await reference!.getDownloadURL());
              print(url);
              SubjectFirebaseController().storeDocument(data: subject, event: (status){

              },parentId: widget.classId);
              print(url);
            } else {
              if (taskState == TaskState.error) {
                changeProgressValue(0);
                showSnackBar(
                    context: context,
                    message: 'Upload failed, try again',
                    error: true);
              } else {
                changeProgressValue(null);
              }
            }
          },
        );
      }
    } else {
      print('CURRENTLY UPLOADING FILE');
    }
  }

  void changeProgressValue(double? value) {
    setState(() {
      _progressValue = value;
    });
  }
}
