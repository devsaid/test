class Subject {
  late String id;
  late String image;
  late String title;
  late String description;

  Subject();

  Subject.fromMap(Map<String, dynamic> documentMap) {
    title = documentMap['title'];
    description = documentMap['description'];
    id = documentMap['subject_id'];
    image = documentMap['image'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String,dynamic>();
    map['title'] = title;
    map['description'] = description;
    map['image'] = image;
    return map;
  }

}