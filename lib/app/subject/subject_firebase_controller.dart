import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:jehad_project/app/subject/subject.dart';
import 'package:jehad_project/firebase_controller.dart';

class SubjectFirebaseController extends FirebaseController<Subject>{
  @override
  Future<void> deleteDocument({required String id, required FirestoreEvent event, String? parentId}) async {
    await firebaseFirestore
        .collection('subject')
        .doc(parentId)
        .collection('subject')
        .doc(id)
        .delete()
        .then((value) => event(true))
        .catchError((error) => event(false));
  }

  @override
  Stream<QuerySnapshot<Object?>> readCollection({String? parentId}) async*{
    yield* firebaseFirestore
        .collection('subject')
        .doc(parentId)
        .collection('subject')
        .snapshots();
  }

  @override
  Stream<DocumentSnapshot<Map<String, dynamic>>> readDocument(String id) {
    // TODO: implement readDocument
    throw UnimplementedError();
  }

  @override
  Future<void> updateDocument({required Subject data, required FirestoreEvent event}) {
    // TODO: implement updateDocument
    throw UnimplementedError();
  }

  @override
  Future<void> storeDocument({required Subject data, String? parentId, required FirestoreEvent event}) async{
    print('-----------------------------------$parentId');
      await firebaseFirestore
          .collection('subject').doc(parentId!).collection('subject')
          .add(data.toMap())
          .then((value) => value.update({'subject_id': value.id}).then((value) => event(true)).catchError((error) => event(false))
      );
  }

}