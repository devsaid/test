import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jehad_project/app/subject/add_subject_screen.dart';
import 'package:jehad_project/app/subject/subject.dart';
import 'package:jehad_project/app/subject/subject_firebase_controller.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jehad_project/widgets/app_text_widget.dart';

class SubjectScreen extends StatelessWidget {

  final String classId ;

  SubjectScreen({required this.classId});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          centerTitle: true,
          title: AppTextWidget(content: 'الـــمـواد',fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold,),
        ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (_) => AddSubjectScreen(classId: classId)));
        },
        child: Icon(Icons.add),
      ),
      backgroundColor: secondaryColor,
      body: StreamBuilder<QuerySnapshot>(
        stream: SubjectFirebaseController().readCollection(parentId: classId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
            List<QueryDocumentSnapshot> docs = snapshot.data!.docs;
                return Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: GridView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: docs.length,
                    shrinkWrap: true,
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 25.h,
                      childAspectRatio: 146.w / 215.h,
                      mainAxisSpacing: 40.w,
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Image.network(docs.elementAt(index).get('image'),),
                            Text(docs.elementAt(index).get('title'),)
                          ],
                        ),
                      );
                    },
            ),
                );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.warning,
                    size: 80,
                  ),
                  Text(
                    'NO DATA',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 28,
                    ),
                  )
                ],
              ),
            );
          }
        },
      )
    );
  }

  Subject getSubject(QueryDocumentSnapshot documentSnapshot) {
    Subject subject = Subject();
    subject.id = documentSnapshot.get('level_id');
    subject.title = documentSnapshot.get('title');
    subject.description = documentSnapshot.get('description');
    subject.image = documentSnapshot.get('image');
    return subject;
  }

}
