import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jehad_project/utils/colors.dart';
import 'package:jehad_project/widgets/app_elevated_button.dart';
import 'package:jehad_project/widgets/app_text_button.dart';

import 'on_boarding_indicator.dart';
import 'on_boarging_widget.dart';


class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  int currentIndex = 0;
  late PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
      body: Stack(
        children: [
          PageView(
            controller: pageController,
            onPageChanged: (int index) {
              setState(() {
                currentIndex = index;
              });
            },
            children: [
              OnBoardingWidget(
                title: 'تطبيق تميز',
                subTitle: 'اهلاً وسهلاً بكم في تطبيق تميز والذي يحتوي على مادة علمية ضخمة',
                image: '23340922',
              ),
              OnBoardingWidget(
                title: 'حقق افضل نتائج',
                subTitle: 'التطبيق يوفر العديد التدريبات والاختبارات الموجوده داخل التطبيق',
                image: '23340923',
              ),
              OnBoardingWidget(
                title: 'وفر الوقت والجهد',
                subTitle: 'تطبيق تميز يوفر عليك الوقت والجهد للوصول للمواد العلمة التي تريدها بسهولة',
                image: '23340921',
              ),
            ],
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Visibility(
              visible: currentIndex != 2,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 45.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AppTextButton(
                        text: 'تخطي',
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        textColor: Colors.black,
                        onPressed: skip),
                    Row(
                      children: [
                        OnBoardingIndicator(
                          isSelected: currentIndex == 0,
                        ),
                        OnBoardingIndicator(
                          isSelected: currentIndex == 1,
                        ),
                        OnBoardingIndicator(
                          isSelected: currentIndex == 2,
                        ),
                      ],
                    ),
                    AppTextButton(
                      text: 'التالي',
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      textColor: primaryColor,
                      onPressed: goToNextPage,
                    ),
                  ],
                ),
              ),
              replacement: Padding(
                padding: EdgeInsets.symmetric(horizontal: 117.w, vertical: 45.h),
                child: AppElevatedButton(
                  onPressed: () => Navigator.pushReplacementNamed(context, 'levels_screen'),
                  text: 'أبدا الآن',
                  buttonColor: primaryColor,
                  fontFamily: 'cairo',
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void skip() {
    pageController.animateToPage(
      2,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void goToNextPage() {
    if (currentIndex == 2) {
      // Get.off(LoginScreen());
    } else {
      pageController.nextPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
      );
    }
  }
}
