import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jehad_project/utils/colors.dart';

class OnBoardingIndicator extends StatelessWidget {
  final bool isSelected;
  OnBoardingIndicator({this.isSelected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.w),
      height: 10.h,
      width: 10.h,
      decoration: BoxDecoration(
        color: isSelected ? primaryColor :secondaryColor,
        shape: BoxShape.circle,
      ),
    );
  }
}
