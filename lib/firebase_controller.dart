import 'package:cloud_firestore/cloud_firestore.dart';

typedef FirestoreEvent = void Function(bool status);

abstract class FirebaseController<T>{

  FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

  Future<void> storeDocument({required T data,String? parentId, required FirestoreEvent event});

  Stream<QuerySnapshot> readCollection({String? parentId});

  Future<void> deleteDocument({required String id, required FirestoreEvent event,String? parentId});

  Future<void> updateDocument({required T data, required FirestoreEvent event});

  Stream<DocumentSnapshot<Map<String, dynamic>>> readDocument(String id);

}