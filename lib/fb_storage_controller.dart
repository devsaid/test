import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

typedef FbUploadEvent = void Function(
    bool status, TaskState state, Reference? reference);

class FbStorageController {
  FirebaseStorage _firebaseStorage = FirebaseStorage.instance;

  Future<List<Reference>> getImages() async {
    ListResult listResult = await _firebaseStorage.ref('images/').listAll();
    if (listResult.items.isNotEmpty) {
      return listResult.items;
    }
    return [];
  }

  Future<void> uploadImage(
      {required File file, required FbUploadEvent uploadEvent}) async {
    UploadTask task =
        _firebaseStorage.ref('images/${DateTime.now()}_image').putFile(file);
    task.snapshotEvents.listen((event) {
      if (event.state == TaskState.running) {
        uploadEvent(false, event.state, null);
      } else if (event.state == TaskState.success) {
        uploadEvent(true, event.state, task.snapshot.ref);
      } else if (event.state == TaskState.error) {
        uploadEvent(false, event.state, null);
      }
    }, onError: (error) {}, onDone: () {
      print('---------------------------------');
      print(task.snapshot.ref.fullPath);
    });
  }

  Future<bool> deleteImage(String reference) async {
    return await _firebaseStorage
        .ref(reference)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }
}
